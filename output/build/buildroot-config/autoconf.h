/*
 * Automatically generated C config: don't edit
 * Buildroot 2013.02 Configuration
 */
#define BR2_ZCAT "gzip -d -c"
#define BR2_HOST_DIR "$(BASE_DIR)/host"
#define BR2_STRIP_EXCLUDE_DIRS ""
#define BR2_avr 1
#define BR2_GNU_MIRROR "http://ftp.gnu.org/pub/gnu"
#define BR2_HG "hg"
#define BR2_JLEVEL 0
#define BR2_STRIP_EXCLUDE_FILES ""
#define BR2_TOOLCHAIN_BUILDROOT 1
#define BR2_SSH "ssh"
#define BR2_BZCAT "bzcat"
#define BR2_STRIP_strip 1
#define BR2_KERNEL_MIRROR "http://www.kernel.org/pub/"
#define BR2_PRIMARY_SITE ""
#define BR2_LOCALFILES "cp"
#define BR2_GIT "git"
#define BR2_DEBIAN_MIRROR "http://ftp.debian.org"
#define BR2_TAR_OPTIONS ""
#define BR2_OPTIMIZE_S 1
#define BR2_BZR "bzr"
#define BR2_XZCAT "xzcat"
#define BR2_HAVE_DOT_CONFIG 1
#define BR2_SCP "scp"
#define BR2_WGET "wget --passive-ftp -nd -t 3"
#define BR2_PACKAGE_OVERRIDE_FILE "$(TOPDIR)/local.mk"
#define BR2_DL_DIR "$(TOPDIR)/dl"
#define BR2_SVN "svn"
#define BR2_DEFCONFIG "$(CONFIG_DIR)/defconfig"
#define BR2_BACKUP_SITE "http://sources.buildroot.net/"
